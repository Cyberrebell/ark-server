# ark-server


## Installation
Create docker-compose.override.yml for configuration

```
version: '2'
services:
  server:
    environment:
      - SERVER_NAME=<your-server>
      - SERVER_PASSWORD=<your-password>
      - SERVER_ADMIN_PASSWORD=<your-admin-password>
      - GAME_MAP=TheIsland
```

## Run
 ```docker-compose up -d```
 