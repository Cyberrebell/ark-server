#!/usr/bin/env bash
set -e
[[ ${AUTO_UPDATE} == 1 ]] && ./update.sh
./ShooterGame/Binaries/Linux/ShooterGameServer ${GAME_MAP}?listen?SessionName=${SERVER_NAME}?ServerPassword=${SERVER_PASSWORD}?ServerAdminPassword=${SERVER_ADMIN_PASSWORD}?RCONEnabled=true?RCONPort=27021 -server -log -NoBattlEye -crossplay
